import json
import csv
from pprint import pprint

with open('allproduct.json') as data_file:    
    data = json.load(data_file)


keys = data["product_detail"][0].keys()
with open('cottonink.csv', 'w') as output_file:
	dict_writer = csv.DictWriter(output_file, keys)
	dict_writer.writeheader()
	dict_writer.writerows(data["product_detail"])