from scrapy.spiders import BaseSpider
import scrapy
from bs4 import BeautifulSoup
import json 


class ModclothSpider(BaseSpider):
    name = "modcloth"

    def start_requests(self):
        url = 'https://www.modcloth.com/style-gallery/api/outfits.json?page=1'

        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        
        data = json.loads(response.body)   
        outfits = data["outfits"]
        for outfit in outfits:
            display_name = outfit["contributor"]["display_name"]
            image_url = outfit["images"]["original"]["url"]
            outfit_url = outfit["_links"]["html"]
            source = "modcloth"
            # ret_outfit = OutfitItem(display_name=display_name, image_url=image_url, product_url=outfit_url, source=source)
            
            ret_outfit = {
                'display_name' : display_name,
                'image_url' : image_url,
                'product_url' : outfit_url,
                'source' : source,

            }

            yield ret_outfit

            for product in outfit["products"]:
                display_name = product["name"]
                image_url = product["images"]["small"]["url"]
                product_url = product["_links"]["html"]
                base_price = float(product["price"][1:])
                currency = "USD"
                source = "modcloth"
                # outfit = Outfit.objects.get(product_url=outfit_url)
                # product = ProductItem(display_name= display_name, image_url=image_url, product_url=product_url, base_price=base_price, currency=currency, outfit=outfit, source=source)

                # product = ProductItem(display_name= display_name, image_url=image_url, product_url=product_url, base_price=base_price, currency=currency, source=source)

                product = {
                    'display_name' : display_name,
                    'image_url' : image_url,
                    'product_url' : product_url,
                    'base_price' : base_price,
                    'currency' : currency,
                    'source' : source,
                }
                yield product

        if (data["pagination"]["total_items"] != 0):
            next_page = 'https://www.modcloth.com/style-gallery/api/outfits.json?page=' + str(int(response.url[61:]) + 1)
            yield scrapy.Request(url=next_page, callback=self.parse)

