# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
from scrapy_djangoitem import DjangoItem
from app.models import ExampleDotCom, Outfit, Product

class ExampleDotComItem(DjangoItem):
    django_model = ExampleDotCom

class OutfitItem(DjangoItem):
    django_model = Outfit

class ProductItem(DjangoItem):
    django_model = Product
