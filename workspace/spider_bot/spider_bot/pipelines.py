# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from app.models import ExampleDotCom, Outfit, Product


class ExPipeline(object):
    def process_item(self, item, spider):
        # try:
        #     Outfit.objects.get(display_name=item["display_name"])
        #     item.update()
        #     return
        # except app.models.DoesNotExist:
        #     print("\n\nNew Product!\n\n")
           #  item.save()
           #  return item
        outfit = Outfit.objects.filter(product_url=item["product_url"])
        if (len(outfit) == 0):
            item.save()
            return item
        else:
            return