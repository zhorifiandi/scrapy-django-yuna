from scrapy.spiders import BaseSpider
from spider_bot.items import OutfitItem, ProductItem
import scrapy
from bs4 import BeautifulSoup
import json 
from app.models import Outfit


class ModclothSpider(BaseSpider):
    name = "modcloth"

    def start_requests(self):
        url = 'https://www.modcloth.com/style-gallery/api/outfits.json?page=1'

        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        
        data = json.loads(response.body)   
        outfits = data["outfits"]
        for outfit in outfits:
            display_name = outfit["contributor"]["display_name"]
            image_url = outfit["images"]["original"]["url"]
            outfit_url = outfit["_links"]["html"]
            source = "modcloth"
            ret_outfit = OutfitItem(display_name=display_name, image_url=image_url, product_url=outfit_url, source=source)
            yield ret_outfit

            for product in outfit["products"]:
                display_name = product["name"]
                image_url = product["images"]["small"]["url"]
                product_url = product["_links"]["html"]
                base_price = float(product["price"][1:])
                currency = "USD"
                source = "modcloth"
                outfit = Outfit.objects.get(product_url=outfit_url)
                product = ProductItem(display_name= display_name, image_url=image_url, product_url=product_url, base_price=base_price, currency=currency, outfit=outfit, source=source)
                yield product

        if (data["pagination"]["total_items"] != 0):
            next_page = 'https://www.modcloth.com/style-gallery/api/outfits.json?page=' + str(int(response.url[61:]) + 1)
            yield scrapy.Request(url=next_page, callback=self.parse)

    #             request = scrapy.Request(url=product_url, callback=self.parse_products)
    #             request.meta['product_url'] = product_url
    #             yield request

    #     if (data['.passback']['next_token']):
    #         next_url = 'https://www.Modcloth.com/cgi/search.sets?.in=json&.out=jsonx&request={"item_count.to":"10000","date":"day","item_count.from":"4","page":null,".passback":{"next_token":{"limit":"30","start":'+str(data['.passback']['next_token']['start'])+'}'+'}'+'}'
    #         yield scrapy.Request(url=next_url, callback=self.parse)

    # def parse_products(self,response):
    #     resources = response.css("ul.layout_grid.grid_5 img")
    #     prices = response.css("ul.layout_grid.grid_5 div.under")
    #     links = response.css("ul.layout_grid.grid_5 div.main a::attr(href)").extract()
    #     for idx, product in enumerate(resources):
    #         display_name = product.css("img::attr(alt)").extract_first()
    #         image_url = product.css("img::attr(src)").extract_first()
    #         product_url = response.url + "/" + str(links[idx])
    #         if (len(prices[idx].css("span.price")) == 0):
    #         	base_price = 0
    #         else:
    #         	base_price = int(prices[idx].css("span.price::text").extract_first()[:-4].replace(".",""))

    #         outfit = Outfit.objects.get(product_url=response.meta['product_url'])

    #         product = ProductItem(display_name= display_name, image_url=image_url, product_url=product_url, base_price=base_price, outfit=outfit)
    #         yield product

