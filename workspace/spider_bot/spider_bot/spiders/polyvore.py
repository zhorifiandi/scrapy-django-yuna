from scrapy.spiders import BaseSpider
from spider_bot.items import OutfitItem, ProductItem
import scrapy
from bs4 import BeautifulSoup
import json 
from app.models import Outfit


class PolyvoreSpider(BaseSpider):
    name = "polyvore"

    def start_requests(self):
        url = 'https://www.polyvore.com/cgi/search.sets?.in=json&.out=jsonx&request={"item_count.to":"10000","date":"day","item_count.from":"4","page":null,".passback":{"next_token":{"limit":"30","start":0'+'}'+'}'+'}'

        yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        
        data = json.loads(response.body)   
        soup = BeautifulSoup(data['result']['html'], 'html.parser')
        for item in soup.find_all('li'):
            if (item.select('img')):
                image = item.select('img')[0]['src']
                display_name = item.select('img')[0]['title']
                product_url = 'https://www.polyvore.com/outfits/' + item.select('a')[0]['href']
                source = "polyvore"
                outfit = OutfitItem(display_name=display_name, image_url=image, product_url=product_url, source=source)
                yield outfit

                request = scrapy.Request(url=product_url, callback=self.parse_products)
                request.meta['product_url'] = product_url
                yield request

                # print("\n\n\n")
                # print(dir(outfit))
                # print(outfit.django_model.pk)

        # print("\n\n\n"+data['result']+"\n\n\n")
        if (data['.passback']['next_token']):
            next_url = 'https://www.polyvore.com/cgi/search.sets?.in=json&.out=jsonx&request={"item_count.to":"10000","date":"day","item_count.from":"4","page":null,".passback":{"next_token":{"limit":"30","start":'+str(data['.passback']['next_token']['start'])+'}'+'}'+'}'
            yield scrapy.Request(url=next_url, callback=self.parse)

    def parse_products(self,response):
        resources = response.css("ul.layout_grid.grid_5 img")
        prices = response.css("ul.layout_grid.grid_5 div.under")
        links = response.css("ul.layout_grid.grid_5 div.main a::attr(href)").extract()
        for idx, product in enumerate(resources):
            display_name = product.css("img::attr(alt)").extract_first()
            image_url = product.css("img::attr(src)").extract_first()
            product_url = response.url + "/" + str(links[idx])
            if (len(prices[idx].css("span.price")) == 0):
            	base_price = 0
            else:
            	base_price = int(prices[idx].css("span.price::text").extract_first()[:-4].replace(".",""))

            outfit = Outfit.objects.get(product_url=response.meta['product_url'])
            source = "polyvore"
            product = ProductItem(display_name= display_name, image_url=image_url, product_url=product_url, base_price=base_price, outfit=outfit, source=source)
            yield product

