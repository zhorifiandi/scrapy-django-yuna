# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from app.models import ExampleDotCom, Outfit, Product

admin.site.register(ExampleDotCom)
admin.site.register(Outfit)
admin.site.register(Product)
