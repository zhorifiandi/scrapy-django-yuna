# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class ExampleDotCom(models.Model):
    title = models.CharField(max_length=255)
    image_url = models.CharField(max_length=255)
    item_url = models.CharField(max_length=255)

    def __str__(self):
        return self.title

class Outfit(models.Model):
    display_name = models.CharField(max_length=255)
    image_url = models.URLField(blank=True, null=True)
    product_url = models.URLField(blank=True, null=True)
    source = models.CharField(max_length=255)

    def __str__(self):
        return self.display_name

class Product(models.Model):
    display_name = models.CharField(max_length=150)
    image_url = models.URLField(blank=True, null=True)
    product_url = models.URLField(blank=True, null=True)
    currency =  models.CharField(max_length=15)
    base_price = models.DecimalField(max_digits=20, decimal_places=2)
    outfit = models.ForeignKey(Outfit)
    source = models.CharField(max_length=255)

    def __str__(self):
        return self.display_name

# class Product(BaseModelGeneric):
#     company = models.ForeignKey(Company, blank=True, null=True)
#     seller_brand = models.ForeignKey(Brand, related_name="%(app_label)s_%(class)s_seller")
#     owner_brand = models.ForeignKey(Brand, related_name="%(app_label)s_%(class)s_owner")
#     merchant_category = models.TextField(blank=True, null=True)
#     merchant_spu = models.CharField(max_length=100)
#     internal_spu = models.CharField(max_length=100)
#     display_name = models.CharField(max_length=150)
#     short_name = models.SlugField(max_length=150)
#     product_url = models.URLField(blank=True, null=True)
#     base_price = models.DecimalField(max_digits=20, decimal_places=2)
#     photo = models.ImageField(
#         storage = storage.STORAGE_PHOTO,
#         upload_to = storage.generate_name,
#         blank = True,
#         null = True
#     )
#     weight_amount = models.FloatField()
#     weight_unit = models.PositiveIntegerField(choices=WEIGHT_UNIT_CHOICES)
#     packing_width = models.FloatField(blank=True, null=True)
#     packing_depth = models.FloatField(blank=True, null=True)
#     packing_length = models.FloatField(blank=True, null=True)
#     is_available = models.BooleanField(default=True)
#     tags = TagManager(through=Tagged)

#     def __unicode__(self):
#         return self.display_name

#     class Meta:
#         verbose_name = _("Product")
#         verbose_name_plural = _("Products")