import sys, os, csv

DJANGO_PROJECT_PATH = '/Users/zhorifiandi/Documents/PARTTIME/yuna/scrapy-django-yuna/workspace/yuna_scraper'
DJANGO_SETTINGS_MODULE = 'yuna_scraper.settings'

sys.path.insert(0, DJANGO_PROJECT_PATH)
os.environ['DJANGO_SETTINGS_MODULE'] = DJANGO_SETTINGS_MODULE

import django

django.setup()


from app.models import Outfit, Product


# write your header first
for obj in Outfit.objects.all():
    row = ""
    for field in obj._meta.fields:
         row += str(getattr(obj, field.name)) + ","
    print(row)